package crarm

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
)

//renvoie la chaine préparée
func structToSQLInsert(dataslice []Sqlizable) (string, error) {
	//si le slice est vide on ne fait rien
	if len(dataslice) == 0 {
		return "", errors.New("slice fourni vide")
	}
	//creation de la chaine avec les attributs
	ts := reflect.TypeOf(dataslice[0]).Elem()
	fmt.Println("type de dataslice[0]", ts)
	table := ts.Name()
	fmt.Println("insertion dans la table ", table)
	prefixe := strings.ToLower(table)
	chaine := "INSERT INTO " + table + " ("
	//peuplement du nom des attributs
	for i := 0; i < ts.NumField(); i++ {
		chaine = chaine + prefixe + "_" + ts.Field(i).Name + ", "
	}

	chaine = strings.TrimRight(chaine, ", ")
	chaine += ") VALUES ("
	//peuplement des valeurs 
	for i := 1; i <= ts.NumField(); i++ {
		chaine += fmt.Sprintf("$%d, ", i)
	}
	chaine = strings.TrimRight(chaine, ", ")
	chaine += ")"// RETURNING "+prefixe+"_"+ts.Field(0).Name
	//creation de la chaine
	fmt.Println("RES : ", chaine)
	return chaine, nil
}

func structToSQLCreate(s Sqlizable) (string, string, error) {
	ts := reflect.TypeOf(s).Elem()
	table := ts.Name()
	prefixe := strings.ToLower(table)
	//debut de la chaine SQL
	chaine := "CREATE TABLE " + table + " ("
	//ajout des champs
	for k := 0; k < ts.NumField(); k++ {
		chaine += prefixe + "_" + ts.Field(k).Name + " " + ts.Field(k).Tag.Get("pg") + ","
	}
	//finalisation de la chaine
	chaine = strings.TrimRight(chaine, ",")
	chaine += ")"
	return table, chaine, nil
}

//CreerTable crée une table SQL à partir d'une structure annotée
func CreerTable(db *sql.DB, c Sqlizable) error {
	//génération de la chaine SQL par reflexion
	table, requete, err := structToSQLCreate(c)
	if err != nil {
		return err
	}
	//création de la table
	tr, err := db.Begin()
	if err != nil {
		fmt.Println("impossible de démarrer la transaction :", err)
		return err
	} else {
		fmt.Println("creation de la table pour la structure ", table, "avec la requete ", requete)

		_, err = tr.Exec("DROP TABLE IF EXISTS " + table + " CASCADE;")
		if err != nil {
			fmt.Println("impossible de détruire la table "+table+" :", err)
			tr.Rollback()
			return err
		}
		res, err := tr.Exec(requete)
		if err != nil {
			fmt.Println("impossible de créer la table "+table+" :", err)
			tr.Rollback()
			return err
		} else {
			fmt.Println("Création de la table "+table+"  avec l'Id : ", res)

			err = tr.Commit()
			if err != nil {
				fmt.Println("impossible de commiter la transaction :", err)
			}
			return err
		}

		panic("le commit a fonctionné ou non, mais on ne peut pas arriver là !")
	}
	panic("ifelse non atteignable")
}

//InsererDanstable reçoit un slice de données à insérer
func InsererDansTable(db *sql.DB, data []Sqlizable) error {
	//création de la chaine de requête préparée
	chaineprep, err := structToSQLInsert(data)
	if err != nil {
		return err
	}
	tr, err := db.Begin()
	if err != nil {
		fmt.Println("impossible de démarrer la transaction :", err)
		return err
	} else {
		fmt.Println("Insertion avec la chaine préparée ", chaineprep)
		req, err := tr.Prepare(chaineprep)
		if err != nil {
			fmt.Println("impossible de préparer la requête :", err)
			tr.Rollback()
			return err
		}
		for index, structval := range data {
			ts := reflect.ValueOf(structval).Elem()
			sli := make([]interface{}, 0, ts.NumField())
			for i := 0; i < ts.NumField(); i++ {
				sli = append(sli, ts.Field(i).String())
			}
			fmt.Println("ligne ", index, " insertion de ", sli)
			_, err := req.Exec(sli...)
			if err != nil {
				fmt.Println("impossible d'insérer :", err)
				tr.Rollback()
				return err
			}
		}
		//tout a fonctionné, on commite
		fmt.Println("commit de la transaction ")
		err = tr.Commit()
		if err != nil {
			fmt.Println("impossible de commiter la transaction :", err)
		}
		return err
	}
	panic("ifelse non atteignable")
}
