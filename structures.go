package crarm

import (
	"database/sql"
	"fmt"
	"reflect"
)

//Base contient la connexion vers la base SQL et les structures de tables
type Base struct {
	*sql.DB
	Tables []Sqlizable            //liste des tables de l'utilisateur
	Data   map[string][]Sqlizable //contenu des tables 
	//	references []interface{} //references détectées (tables pivots...)
}

func New(dn string, u string, pw string, s string) (*Base, error) {
	db, err := sql.Open("postgres", "dbname="+dn+" user="+u+" password="+pw+" sslmode="+s)
	if err != nil {
		return &Base{}, err
	}
	t := make([]Sqlizable, 0, 10)
	d := make(map[string][]Sqlizable)
	return &Base{db, t, d}, nil

}

func (s *Base) Add(t Sqlizable) {
	ref := reflect.TypeOf(t).Elem().Name()
	s.Data[ref] = append(s.Data[ref], t)

}

func (s *Base) PopulateTable(t Sqlizable) {
	ref := reflect.TypeOf(t).Elem().Name()
	err := InsererDansTable(s.DB, s.Data[ref])
	if err != nil {
		fmt.Println("erreur lors du peuplement de la table ", ref, " :", err)

	} else {
		fmt.Println("peuplement terminé avec succès pour la table", ref)
	}
}
func (s *Base) RegisterTable(t Sqlizable) {
	ref := reflect.TypeOf(t).Elem().Name()
	fmt.Println("enregistrement de la table ", ref)
	//	v:=make(reflect.TypeOf(t).Elem(),20,100)
	//v:= reflect.MakeSlice(reflect.TypeOf(t).Elem(),10,10).Interface().([]Sqlizable)
	s.Tables = append(s.Tables, t)
	s.Data[ref] = make([]Sqlizable, 0, 16)

}

func (s *Base) CreateTables() error {
	fmt.Println("création de ", len(s.Tables), " tables")
	for k, v := range s.Tables {
		fmt.Println("creation de la table ", v)
		err := CreerTable(s.DB, v)
		if err != nil {
			fmt.Println("erreur à la création de la table ", k)
			return err
		} else {
			fmt.Println("Table ", k, " créee")
		}
	}
	return nil

}

//les structures doivent implémenter cette interface pour être intégrées à la base
type Sqlizable interface {
	InDb()
}
